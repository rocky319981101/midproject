import {Component, OnInit} from '@angular/core';
import {StudentService} from '../service/student.service';
import {FormBuilder, Validators} from '@angular/forms';
import {MatSnackBar} from '@angular/material';
import {Router} from '@angular/router';

@Component({
  selector: 'app-student-login',
  templateUrl: './student-login.component.html',
  styleUrls: ['./student-login.component.scss']
})
export class StudentLoginComponent implements OnInit {
  constructor(
    public studentService: StudentService,
    public fb: FormBuilder,
    public snackBar: MatSnackBar,
    public router: Router
  ) {
  }

  loginForm = this.fb.group({
    email: this.fb.control('', [Validators.required, Validators.email]),
    password: this.fb.control('', Validators.required)
  });

  ngOnInit() {
    if (this.studentService.loginStudent) {
      this.router.navigateByUrl('/');
    }
  }

  onSubmit({email, password}: { email: string; password: string }) {
    const ret = this.studentService.login(email, password);
    if (ret.state === 'ok') {
      this.router.navigateByUrl('/student');
    } else {
      alert(ret.msg);
    }
  }
}

