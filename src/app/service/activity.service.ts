import {Injectable} from '@angular/core';
import {Activity, exampleModelData, Student, StudentActivity} from '../model';

@Injectable({
  providedIn: 'root'
})
export class ActivityService {
  public static activitiesKey = 'activities';
  public static studentActivityListKey = 'studentActivityListKey';

  constructor() {
    this.load();
  }

  activities: Activity[] = [];
  studentActivityList: StudentActivity[] = [];

  activitiesByStudentId(studentId: string) {
    return this.studentActivityList
      .filter(x => x.studentId === studentId)
      .map(sa => {
        const a = this.activities.find(x => x.id === sa.activityId)!;
        if (a) {
          return {...a, state: sa.state};
        } else {
          return a;
        }
      })
      .filter(x => !!x);
  }

  apply(activity: Activity, student: Student) {
    this.studentActivityList.push({
      activityId: activity.id,
      studentId: student.id,
      state: 'pending'
    });
    this.save();
  }

  confirmOrPending(activityId: number, teacherId: string, studentId: string) {
    const sa = this.studentActivityList.find(
      x => x.studentId === studentId && x.activityId === activityId
    );
    if (sa) {
      if (sa.state === 'pending') {
        sa.state = 'confirm';
      } else {
        sa.state = 'pending';
      }
    }
    this.save()
  }

  getStatus(studentId: string, activityId: number) {
    const sa = this.studentActivityList.find(
      x => x.activityId === activityId && x.studentId === studentId
    );
    if (sa) {
      return sa.state;
    } else {
      return '';
    }
  }

  add(activity: Activity) {
    this.activities.push(activity);
    this.save();
  }

  isAlreadyApplied(activityId: number, studentId: string) {
    return !!this.studentActivityList.find(
      x => x.activityId === activityId && x.studentId === studentId
    );
  }

  save() {
    localStorage.setItem(
      ActivityService.activitiesKey,
      JSON.stringify(this.activities)
    );
    localStorage.setItem(
      ActivityService.studentActivityListKey,
      JSON.stringify(this.studentActivityList)
    );
  }

  load() {
    const students = localStorage.getItem(ActivityService.activitiesKey);
    const studentActivityList = localStorage.getItem(
      ActivityService.studentActivityListKey
    );
    if (students) {
      this.activities = JSON.parse(students);
    } else {
      this.activities = exampleModelData.activities;
    }
    if (studentActivityList) {
      this.studentActivityList = JSON.parse(studentActivityList);
    } else {
      this.studentActivityList = exampleModelData.studentActivityList;
    }
  }
}
