import {Injectable} from '@angular/core';
import {exampleModelData, Ret, Student} from '../model';

@Injectable({
  providedIn: 'root'
})
export class StudentService {
  public static studentsKey = 'students';

  constructor() {
    this.load();
    // for dev
    // this.loginStudent = this.students[0];
  }

  loginStudent: null | Student = null;

  students: Student[] = [];

  login(email: string, password: string): Ret<{ student?: Student }> {
    const student = this.students.find(x => x.email === email);
    if (student) {
      if (student.password === password) {
        this.loginStudent = student;
        return {state: 'ok', student};
      } else {
        return {state: 'fail', msg: 'Password error.'};
      }
    } else {
      return {state: 'fail', msg: 'Student not exists.'};
    }
  }

  reg(
    name: string,
    surname: string,
    birth: string,
    id: string,
    email: string,
    password: string
  ): Ret<{ student?: Student }> {
    if (this.students.find(x => x.id === id)) {
      return {state: 'fail', msg: 'id exitst'};
    }
    if (this.students.find(x => x.email === email)) {
      return {state: 'fail', msg: 'email exitst'};
    }
    const student: Student = {
      name,
      surname,
      birth,
      id,
      email,
      password,
      confirmed: false
    };
    this.students.push(student);
    this.save();
    return {state: 'ok', student};
  }

  confirmed(id: string) {
    let stu = this.students.find(x => x.id === id);
    if (stu) {
      stu.confirmed = true;
    }
    this.save();
  }

  save() {
    localStorage.setItem(
      StudentService.studentsKey,
      JSON.stringify(this.students)
    );
  }

  load() {
    const students = localStorage.getItem(StudentService.studentsKey);
    if (students) {
      this.students = JSON.parse(students);
    } else {
      this.students = exampleModelData.students;
    }
  }
}