import {Injectable} from '@angular/core';
import {StudentService} from './student.service';
import {ActivityService} from './activity.service';
import {TeacherService} from './teacher.service';

@Injectable({
  providedIn: 'root'
})
export class ManagerService {
  constructor(
    public studentSrv: StudentService,
    public activitySrv: ActivityService,
    public teacherSrv: TeacherService
  ) {
  }

  allStudents() {
    return {
      confirmed: this.studentSrv.students.filter(x => x.confirmed),
      unconfirmed: this.studentSrv.students.filter(x => !x.confirmed)
    };
  }

  confirmStudent(studentId: string) {
    if (window.confirm('Do you really want to confirm this student?')) {
      const s = this.studentSrv.students.find(x => x.id === studentId);
      if (s) {
        s.confirmed = true;
      }
      this.studentSrv.save();
    }
  }

  addActivity(obj: {
    name: string;
    location: string;
    description: string;
    period: string;
    datetime: string;
    teacherId: string;
  }) {
    const id =
      this.activitySrv.activities
        .map(x => x.id)
        .reduce((p, c) => (p < c ? c : p), 1) + 1;
    this.activitySrv.activities.push({...obj, id});
    this.activitySrv.save();
  }
}
