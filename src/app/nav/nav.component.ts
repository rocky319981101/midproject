import { Component, OnInit } from '@angular/core';
import {StudentService} from '../service/student.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  constructor(
    public studentService:StudentService
  ) { }

  ngOnInit() {
  }

}
